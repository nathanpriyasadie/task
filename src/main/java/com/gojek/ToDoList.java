package com.gojek;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


public class ToDoList {
    private List<Task> taskList;

    public void addTaskList(Task task){
        taskList.add(task);
    }
    public String getTaskList(){
        String tasksData = taskList.stream().map(task -> task.getTask()).collect(Collectors.joining("\n"));
        return tasksData;
    }
}
