package com.gojek;

public class Task {
    public Integer id;
    public String name;
    public String status;



    public Task(int id,String name, String status){
        this.id=id;
        this.name=name;
        this.status=status;
    }

    public String getTask(){
        String task = this.id + "." + this.name + " [" + this.status + "]";
        return task;

    }




}
